<?php

namespace App\Command\Group;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(name: 'app:delete-group')]
class DeleteGroupCommand extends Command
{
    private string $serverRoute;

    public function __construct(
        private HttpClientInterface $client,
        private ParameterBagInterface $parameterBag,
    )
    {
        $this->serverRoute = $this->parameterBag->get('serverRoute');
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Delete group')
            ->addArgument('id', InputArgument::REQUIRED, 'Group id');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('id');
        try {
            $response = $this->client->request('DELETE', "{$this->serverRoute}/groups/$id", [
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]);

            if($response->getStatusCode() === 204) {
                $output->writeln('SUCCESS');
                return Command::SUCCESS;
            } else {
                $output->writeln('FAILURE');
                return Command::FAILURE;
            }
        } catch (\Throwable $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }
}