<?php

namespace App\Command\Group;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(name: 'app:edit-group')]
class EditGroupCommand extends Command
{
    private string $serverRoute;

    public function __construct(
        private HttpClientInterface $client,
        private ParameterBagInterface $parameterBag,
        private SerializerInterface $serializer,
    )
    {
        $this->serverRoute = $this->parameterBag->get('serverRoute');
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Edit group')
            ->addArgument('id', InputArgument::REQUIRED, 'Group id')
            ->addArgument('name', InputArgument::OPTIONAL, 'Group name');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('id');
        $data = [];
        if($name = $input->getArgument('name'))  $data['name'] = $name;

        try {
            $response = $this->client->request('PUT', "{$this->serverRoute}/groups/$id", [
                'body' => $this->serializer->serialize($data, JsonEncoder::FORMAT),
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]);

            $output->writeln($response->getContent());
            return $response->getStatusCode() === 200 ? Command::SUCCESS : Command::FAILURE;
        } catch (\Throwable $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }
}