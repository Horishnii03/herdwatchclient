<?php

namespace App\Command\Group;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(name: 'app:get-users-by-group')]
class GetUsersByGroupCommand extends Command
{
    private string $serverRoute;

    public function __construct(
        private HttpClientInterface $client,
        private ParameterBagInterface $parameterBag,
    )
    {
        $this->serverRoute = $this->parameterBag->get('serverRoute');
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Get users by group')
            ->addArgument('id', InputArgument::REQUIRED, 'Group id');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('id');
        try {
            $response = $this->client->request('GET', "{$this->serverRoute}/groups/$id/users", [
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]);

            $data = $response->toArray();
            if($data && !empty($data['hydra:totalItems'])) {
                $output->writeln($response->getContent());
            } else {
                $output->writeln("No users");
            }

            return $response->getStatusCode() === 200 ? Command::SUCCESS : Command::FAILURE;
        } catch (\Throwable $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }
}