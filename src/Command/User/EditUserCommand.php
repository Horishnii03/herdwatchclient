<?php

namespace App\Command\User;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(name: 'app:edit-user')]
class EditUserCommand extends Command
{
    private string $serverRoute;

    public function __construct(
        private HttpClientInterface $client,
        private ParameterBagInterface $parameterBag,
        private SerializerInterface $serializer,
    )
    {
        $this->serverRoute = $this->parameterBag->get('serverRoute');
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Edit user')
            ->addArgument('id', InputArgument::REQUIRED, 'User id')
            ->addArgument('name', InputArgument::OPTIONAL, 'User name')
            ->addArgument('email', InputArgument::OPTIONAL, 'User email')
            ->addArgument('userGroup', InputArgument::OPTIONAL, 'User group');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('id');
        $data = [];
        if($name = $input->getArgument('name'))  $data['name'] = $name;
        if($email = $input->getArgument('email'))  $data['email'] = $email;
        if($userGroup = $input->getArgument('userGroup'))  $data['userGroup'] = "/api/groups/{$userGroup}/users";

        try {
            $response = $this->client->request('PUT', "{$this->serverRoute}/users/$id", [
                'body' => $this->serializer->serialize($data, JsonEncoder::FORMAT),
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]);

            $output->writeln($response->getContent());
            return $response->getStatusCode() === 200 ? Command::SUCCESS : Command::FAILURE;
        } catch (\Throwable $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }
}