<?php

namespace App\Command\User;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(name: 'app:create-user')]
class AddUserCommand extends Command
{
    private string $serverRoute;

    public function __construct(
        private SerializerInterface $serializer,
        private HttpClientInterface $client,
        private ParameterBagInterface $parameterBag,
    )
    {
        $this->serverRoute = $this->parameterBag->get('serverRoute');
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a new user')
            ->addArgument('name', InputArgument::REQUIRED, 'User name')
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
            ->addArgument('userGroup', InputArgument::OPTIONAL, 'User group');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $name = $input->getArgument('name');
        $email = $input->getArgument('email');
        $userGroup = $input->getArgument('userGroup');
        $data = [
            'name' => $name,
            'email' => $email,
            'userGroup' => "/api/groups/{$userGroup}/users"
        ];

        try {
            $response = $this->client->request('POST', "{$this->serverRoute}/users", [
                'body' => $this->serializer->serialize($data, JsonEncoder::FORMAT),
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]);

            $output->writeln($response->getContent());
            return $response->getStatusCode() === 201 ? Command::SUCCESS : Command::FAILURE;
        } catch (\Throwable $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }
}