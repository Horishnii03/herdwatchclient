<?php

namespace App\Command\User;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(name: 'app:get-user')]
class GetUserCommand extends Command
{
    private string $serverRoute;

    public function __construct(
        private HttpClientInterface $client,
        private ParameterBagInterface $parameterBag,
    )
    {
        $this->serverRoute = $this->parameterBag->get('serverRoute');
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Get user')
            ->addArgument('id', InputArgument::REQUIRED, 'User id');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('id');
        try {
            $response = $this->client->request('GET', "{$this->serverRoute}/users/$id", [
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]);

            $output->writeln($response->getContent());
            return $response->getStatusCode() === 200 ? Command::SUCCESS : Command::FAILURE;
        } catch (\Throwable $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }
}