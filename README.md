# HerdwatchClient

Client uses ngrok server. If you want to connect your server, change API_ROUTE in .env

## Commands
## Group
app:get-group -id

app:get-groups-list

app:get-users-by-group -id

app:create-group -name

app:edit-group -id -name

app:delete-group -id
## User
app:get-user -id

app:get-users-list

app:create-user -name -email -groupId

app:edit-user -id -name -email -groupId

app:delete-user -id